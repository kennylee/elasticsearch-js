# ElasticSearch

前端ElasticSearch实现查询的工具类，例子见 [demo.html](src/html/demo.html)

## 查询说明

* 用户输入多添加时，多个查询条件用空格分割，但如果实际查询条件中带空格，请用双引号("")，括起来。
* 查询条件中，支持"非"操作，例如 `-广州`， 意思是 **不要包含广州** 的内容。

例如:

> 中国 广州 -女 "hello world" -"one two"

查询条件是5个，其中 1，中国。2、广州。3、hello world 为必须包含的内容，但需要过滤掉包含 `女` 和 `one two` 的结果。

## 工具说明

* ElasticSearch

ElasticSearch工具类

```js
/**
* 返回构建的QueryBody实例查询体对象
*
* @param text 用户输入的文本
* @param types 可选，指定查询文档类型(type)
* @returns QueryBody 返回QueryBody实例。
*/
getQueryBody: function (text, types)
```

* QueryBody

查询体对象

```js
/**
* 设置查询范围
*
* @param prop 范围字段名
* @param gte 大于等于
* @param lte 小于等于
* @returns {QueryBody}
*/
setRange: function (prop, gte, lte)

/**
*
* 设置必须检索的条件
*
* @param prop 检索的字段名
* @param query 查询的字段
* @param isPhrase 是否词组匹配
* @param operator 逻辑符，and和or
* @returns {QueryBody}
*/
addMust: function (prop, query, isPhrase, operator)

/**
 *
 * 设置"不"必须检索的条件
 *
 * @param prop 检索的字段名
 * @param query 查询的字段
 * @param isPhrase 是否词组匹配
 * @param operator 逻辑符，and和or
 * @returns {QueryBody}
 */
addMustNot: function (prop, query, isPhrase, operator) {

/**
 *
 * 返回json对象
 *
 * @returns {*}
 */
toJSON: function ()

/**
 *
 * 设置检索的文档类型。
 *
 * @param types 数组
 * @returns {QueryBody}
 */
setTypes: function (types)

/**
 *
 * 设置检索的起始条目。
 *
 * @param index
 * @returns {QueryBody}
 */
setStartIndex: function (index)

/**
 *
 * 设置检索的分页大小。
 *
 * @param size
 * @returns {QueryBody}
 */
setPageSize: function (size)

/**
 *
 * 设置查询时，过滤的source属性。
 *
 * @param arr 数组
 * @returns {QueryBody}
 */
setExcludesProps: function (arr)

/**
 * 返回JSON的字符串
 *
 * @returns String
 */
toString: function ()
```